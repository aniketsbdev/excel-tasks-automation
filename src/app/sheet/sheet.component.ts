import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { slideInAnimation } from '../animations';
import { Router } from '@angular/router';
import * as XLSX from 'xlsx';
import { NetworkCallsService } from '../network/network-calls.service'
import { FormControl } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { MatSelectChange } from '@angular/material/select';


@Component({
  selector: 'app-sheet',
  templateUrl: './sheet.component.html',
  styleUrls: ['./sheet.component.scss'],
  animations: [slideInAnimation]
})
export class SheetComponent implements OnInit {

  dataLoaded: Boolean = false;
  masterData: any = [];
  serialNumber: FormControl = new FormControl();
  invoiceNumber: FormControl = new FormControl();
  orderNumber: FormControl = new FormControl();
  permitNumber: FormControl = new FormControl();
  partyName: FormControl = new FormControl();
  department: FormControl = new FormControl();
  salesMan: FormControl = new FormControl();
  itemCode: FormControl = new FormControl();
  size: FormControl = new FormControl();
  quantity: FormControl = new FormControl();
  billValue: FormControl = new FormControl();
  vatAmount: FormControl = new FormControl();
  exciseRevenue: FormControl = new FormControl();
  tcsAmount: FormControl = new FormControl();

  serialNumberCtrl: FormControl = new FormControl();
  filteredSerialNumber: ReplaySubject<[]> = new ReplaySubject<[]>(1);
  _onDestroy = new Subject<void>();

  constructor(private router: Router,
    private network: NetworkCallsService,
    private ref: ChangeDetectorRef) {
  }

  goToLogin() {
    this.router.navigateByUrl('');
  }

  getFile() {
    this.network.getExcelFile().subscribe(data => {
      var newData = new Uint8Array(data);
      var arr = new Array();
      for (var i = 0; i != newData.length; ++i) arr[i] = String.fromCharCode(newData[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      this.masterData = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      console.log(this.masterData);
    });

    this.dataLoaded = true;
  }

  ngOnInit(): void {

    // this.serialNumberCtrl.valueChanges
    //   .pipe(takeUntil(this._onDestroy))
    //   .subscribe(() => {
    //     this.filterBanks();
    //   });
  }

  getData(event: MatSelectChange) {
    console.log(event.value);
    this.invoiceNumber.setValue(event.value.InvoiceNumber);
    this.orderNumber.setValue(event.value.OrderNumber);
    this.permitNumber.setValue(event.value.PermitNumber);
    this.partyName.setValue(event.value.PartyName);
    this.department.setValue(event.value.Department);
    this.salesMan.setValue(event.value.SalesMan);
    this.itemCode.setValue(event.value.ItemCode);
    this.size.setValue(event.value.Size);
    this.quantity.setValue(event.value.Quantity);
    this.billValue.setValue(event.value.BillValue);
    this.exciseRevenue.setValue(event.value.ExciseRevenue);
    this.vatAmount.setValue(event.value.VatAmount);
    this.tcsAmount.setValue(event.value.TCSAmount);
  }

  // protected filterBanks() {
  //   if (!this.masterData) {
  //     return;
  //   }
  //   // get the search keyword
  //   let search = this.serialNumberCtrl.value;
  //   if (!search) {
  //     this.filteredSerialNumber.next(this.masterData.slice());
  //     return;
  //   } else {
  //     search = search.toLowerCase();
  //   }
  //   // filter the banks
  //   this.filteredSerialNumber.next(
  //     this.masterData.filter(transaction => transaction.Sno.toLowerCase().indexOf(search) > -1)
  //   );
  // }

  // ngOnDestroy() {
  //   this._onDestroy.next();
  //   this._onDestroy.complete();
  // }

}
