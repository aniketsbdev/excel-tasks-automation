import { Component, OnInit } from '@angular/core';
import { slideInAnimation } from 'src/app/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  animations: [slideInAnimation]
})
export class AuthComponent implements OnInit {

  constructor(private router: Router) { }

  login() {
    this.router.navigateByUrl('/home');
  }

  ngOnInit(): void {
  }


}
