import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as XLSX from 'xlsx';

@Injectable({
  providedIn: 'root'
})
export class NetworkCallsService {

  constructor(private http: HttpClient) { }

  getExcelFile(): any {
    return this.http.get('assets/excel-data/master-data-small.xlsx', { responseType: 'arraybuffer' }).pipe(res => res);
      
  }
}